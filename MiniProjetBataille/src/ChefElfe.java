public class ChefElfe extends Guerrier{
    private static final int forceChefElfe = 4;
    private static final int cout = 4;
    @Override
    public int getForce(){
        return super.getForce() * forceChefElfe;
    }
    @Override
    public int getRessourcePourEntrainement() {
        return cout;
    }
}
