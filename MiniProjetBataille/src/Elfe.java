public class Elfe extends Guerrier{
    private static final int cout = 2;
    private static final int forceElfe = 2;
    @Override
    public int getForce(){
        return super.getForce() * forceElfe;
    }
    @Override
    public int getRessourcePourEntrainement() {
        return cout;
    }
}
