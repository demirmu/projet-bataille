import java.util.LinkedList;

public class Carreau {
    private LinkedList<Guerrier> guerrierBleu;
    private LinkedList<Guerrier> guerrierRouge;

    Carreau(){

        guerrierRouge = new LinkedList<Guerrier>();
        guerrierBleu = new LinkedList<Guerrier>();
    }
    //GETTER
    public LinkedList<Guerrier> getGuerrierBleu(){
        return guerrierBleu;
    }
    public LinkedList<Guerrier> getGuerrierRouge(){
        return guerrierRouge;
    }

    //SETTER
    public void setGuerrierBleu(LinkedList<Guerrier> guerrierBleu) {this.guerrierBleu = guerrierBleu;}
    public void setGuerrierRouge(LinkedList<Guerrier> guerrierRouge) {this.guerrierRouge = guerrierRouge;}


    public void ajoutGuerrierBleu(LinkedList<Guerrier> guerrierBleuAjout){
        if(guerrierBleuAjout != null){
            guerrierBleu.addAll(guerrierBleuAjout); // ajoute les guerrierBleu à la fin de la liste des  guerriers Bleu
        }

    }
    public void ajoutGuerrierRouge(LinkedList<Guerrier> guerrierRougeAjout){
        if (guerrierRougeAjout != null){
            guerrierRouge.addAll(guerrierRougeAjout);
        }
    }

    public LinkedList<Guerrier> retirerGuerrierBleu() {
        LinkedList<Guerrier> guerrierBleuRetirer = getGuerrierBleu();
        getGuerrierBleu().clear();
        return guerrierBleuRetirer;
    }

    public LinkedList<Guerrier> retirerGuerrierRouge(){
        LinkedList<Guerrier> guerrierRougeRetirer = getGuerrierRouge();
        getGuerrierRouge().clear();
        return guerrierRougeRetirer;
    }

    public void supprimerGuerrier(Guerrier guerrier){
        if(guerrierBleu.contains(guerrier)){
            guerrierBleu.remove(guerrier);
        }
        if(guerrierRouge.contains(guerrier)){
            guerrierRouge.remove(guerrier);
        }

    }

    public boolean estRouge() {
        return getGuerrierRouge().size() > 0;
    }

    public boolean estBleu(){
        return getGuerrierBleu().size() > 0;
    }
    public boolean estChampDeBataille(){
      /*  if (!guerrierBleu.isEmpty() && !guerrierRouge.isEmpty()){
            return true;
        }else{
            return false;
        }*/
        return estBleu() && estRouge();
    }

    public void lancerCombat(){

    }


}
