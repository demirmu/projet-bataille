import java.util.Random;

public class desTroisUtilitaire {

    public static int desTrois() {
        int min = 1;
        int max = 3;
        Random random = new Random();
        int valeur = random.nextInt(max) + min;

        return valeur;

    }

    public static int desTrois(int nblance) {
        int nb = 0;
        int att = 0;
        while(nb < nblance){
            att += desTrois();
            nb ++;
        }
        System.out.println(att);
        return att;
    }
}
