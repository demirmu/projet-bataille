import java.util.LinkedList;
import java.util.Random;

public class TestEtape1 {
    public static void main(String[] args) {
        ///////////// ETAPE 1 ////////////////
        /*Guerrier guerrier1 = new Guerrier();
        Guerrier guerrier2 = new Guerrier();
        System.out.println("Pv guerrier2 : " + guerrier2.getPv());

        // test guerrier attack guerrier
       int nb = 0;
        while(guerrier2.estVivant() == true && guerrier1.estVivant() == true){
            guerrier2.attaquer(guerrier1);
            System.out.println("Pv1 : " +guerrier1.getPv());
            if(guerrier1.estVivant() == true){
                guerrier1.attaquer(guerrier2);
                System.out.println("Pv2 : " +guerrier2.getPv());
            }

            nb ++;
        }
        if(guerrier1.estVivant() == false){
            System.out.println("MORT 1");
        }else{
            System.out.println("MORT 2");
        }
        System.out.println("Attack : "+nb);

        // test des différents type de guerrier

        Nain nain = new Nain();
        ChefNain chefNain = new ChefNain();
        Elfe elfe = new Elfe();
        ChefElfe chefElfe = new ChefElfe();

        chefElfe.attaquer(chefNain);
        System.out.println(nain.getPv());*/

        //////////////// ETAPE 2 /////////////////
        Nain nain1 = new Nain();
        Nain nain2 = new Nain();
        Elfe elfe1 = new Elfe();
        Elfe elfe2 = new Elfe();
        ChefNain chefNain1 = new ChefNain();
        Chateau chateauB = new Chateau();
        Chateau chateauA = new Chateau();

        chateauB.ajoutGuerrierNovice(nain1);
        chateauB.ajoutGuerrierNovice(elfe1);
        chateauB.ajoutGuerrierNovice(elfe2);

        chateauA.ajoutGuerrierNovice(nain1);
        chateauA.ajoutGuerrierNovice(elfe1);
        chateauA.ajoutGuerrierNovice(elfe2);

        LinkedList<Guerrier> guerrierListeTestB = new LinkedList<>();
        guerrierListeTestB = chateauB.entrainer();

        LinkedList<Guerrier> guerrierListeTestR = new LinkedList<>();
        guerrierListeTestR = chateauA.entrainer();

        for(Guerrier g : guerrierListeTestB){
            System.out.println("Guerriers Bleu : "+ g);
        }
        for(Guerrier g : guerrierListeTestR){
            System.out.println("Guerriers Rouge : "+g);
        }

        //////////////// ETAPE 3 : CAREAUX //////////////////////

        Carreau carreau = new Carreau();
        /*carreau.ajoutGuerrierBleu(guerrierListeTestB);
        System.out.println("Il y a "+carreau.getGuerrierBleu()+ " sur cette case ");
        System.out.println("Il y a "+carreau.getGuerrierRouge()+ " sur cette case ");

        carreau.ajoutGuerrierRouge(guerrierListeTestR);
        System.out.println("L'ajout de l'équipe Rouge sur la case");
        System.out.println("Il y a "+carreau.getGuerrierBleu()+ " sur cette case ");
        System.out.println("Il y a "+carreau.getGuerrierRouge()+ " sur cette case ");

        carreau.retirerGuerrierBleu();
        System.out.println("On retire l'équipe bleu de la case");
        System.out.println("Il y a "+carreau.getGuerrierBleu()+ " sur cette case ");
        System.out.println("Il y a "+carreau.getGuerrierRouge()+ " sur cette case ");

        carreau.retirerGuerrierRouge();
        System.out.println("On retire l'équipe Rouge de la case");
        System.out.println("Il y a "+carreau.getGuerrierBleu()+ " sur cette case ");
        System.out.println("Il y a "+carreau.getGuerrierRouge()+ " sur cette case ");*/

        ////////// ETAPE 3 : PLATEAU ////////////////

        Plateau plateau = new Plateau(5);

        plateau.ajouterGuerriersBleu(guerrierListeTestB);
        plateau.deplaceGuerrier();

    }
}



