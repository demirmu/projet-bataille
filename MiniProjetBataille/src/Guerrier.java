import java.awt.*;

public abstract class Guerrier {

    private static final int pointsDeVie = 100;
    private static final int force = 10;
    private static final int ressource_base = 1;
    private int pv;

    Guerrier(){
        pv = pointsDeVie;
    }
    public int getForce(){
        return this.force;
    }
    public int getPointsDeVie() {
        return this.pointsDeVie;
    }
    public int getPv() {
        return pv;
    }

    public int getRessourcePourEntrainement() {
        return ressource_base;
    }

    public void setPv(int pv) { this.pv = pv;}
    public void setPointsDeVie(int pointsDeVie) {
        pointsDeVie = this.pointsDeVie;
    }
    public void setChateau(Chateau chateau){

    }
    public boolean estVivant(){
        if(pv > 0){
            return true;
        }else{
            return false;
        }
    }
    public void attaquer(Guerrier guerrier){
       int degat =  guerrier.subirDegats(desTroisUtilitaire.desTrois(getForce()));// je fais subir le total des dés au guerrier, celon sa force
        System.out.println("Degat : "+ degat);
    }

    protected int subirDegats(int degats){
        setPv(getPv() - degats);
        return degats;
    }

    @Override
    public String toString() {
        return getClass().getName() + "{" +
                "pv=" + pv +
                '}';
    }
}
