import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Chateau {

    private static final int ressource_initial = 3;
    private static final int ressource_ajoutee_par_tour = 1;
    private int resource = 3;
    private LinkedList<Guerrier> guerrierListe = new LinkedList<>();

    Chateau(){
    }

    public void ajoutGuerrierNovice(Guerrier guerrier){
        guerrierListe.addLast(guerrier);
    }
    public LinkedList<Guerrier> getGuerrier(){
        return guerrierListe;
    }
    public LinkedList<Guerrier> entrainer(){
        int i =0;
        LinkedList<Guerrier> guerrierEntraine = new LinkedList<>();
        while(guerrierListe.size() > 0 && guerrierListe.peekFirst().getRessourcePourEntrainement() <= resource ){
            // si il rentre cela veut dire que la liste n'est pas vide et le cout du premier et infèrieure ou égale au nombre de ressource

            resource = resource - guerrierListe.peekFirst().getRessourcePourEntrainement();// je supprime la ressource au cout du guerrier
            Guerrier premierGuerrier = guerrierListe.pollFirst();// je supprime la premiere personne dans le tableau

            guerrierEntraine.add(premierGuerrier);

        }
        resource = ressource_initial;

        return guerrierEntraine;

    }

    public int incrementerRessources(){
        return resource = resource + ressource_ajoutee_par_tour;
    }


}
