public class Nain extends Guerrier{
    private static final int cout = 1;
    private static final int degatNain = 2;
    @Override
    protected int subirDegats(int degats){
        return super.subirDegats(degats/degatNain);
    }

    @Override
    public int getRessourcePourEntrainement() {
        return cout;
    }

}
