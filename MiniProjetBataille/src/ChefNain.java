public class ChefNain extends Guerrier{
    private static final int cout = 3;
    private static final int degatChefNain = 4;
    @Override
    protected int subirDegats(int degats){
        return super.subirDegats(degats/degatChefNain);
    }
    @Override
    public int getRessourcePourEntrainement() {
        return cout;
    }
}
